import React from 'react'
import { observer } from 'mobx-react'

import { StockLogContainer } from './StockLogContainer'
import { StockSummaryContainer } from './StockSummaryContainer'
import { AppComponent } from './AppComponent'

export const AppContainer = observer(
  function AppContainer () {
    return (
      <AppComponent
        logSlot={<StockLogContainer />}
        summarySlot={<StockSummaryContainer />}
      />
    )
  }
)
