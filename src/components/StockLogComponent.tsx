import React from 'react'
import { Button, ButtonGroup, Card } from 'react-bootstrap'

import { LogModelDTO } from '../store/Quote/LogModel'

import { StockLogMessageComponent } from './StockLogMessageComponent'
import { StockLogSymbolComponent } from './StockLogSymbolComponent'

export interface IStockLogProps {
  onPauseToggleClick?: () => void,
  pauseLogLabel?: string,
  unPauseLogLabel?: string,
  messages?: LogModelDTO[],
  isLogging?: boolean
}

export const StockLogComponent:React.FC<IStockLogProps> = ({
  onPauseToggleClick,
  pauseLogLabel = 'pause',
  unPauseLogLabel = 'resume',
  isLogging,
  messages
}) => {

  function handlePauseToggleLogClick (event: React.MouseEvent<HTMLButtonElement>) {
    event.preventDefault()
    event.stopPropagation()
    if (typeof onPauseToggleClick !== 'function') return
    onPauseToggleClick()
  }

  const logMessages = Array.isArray(messages) && messages || []

  return (
    <Card className='h-100'>
      <Card.Header className='d-flex'>
        <h4 className='mr-auto'>Log</h4>
        <ButtonGroup>
          <Button
            variant='secondary'
            onClick={handlePauseToggleLogClick}
          >
            {
              isLogging
                ? pauseLogLabel
                : unPauseLogLabel
            }
          </Button>
        </ButtonGroup>
      </Card.Header>
      <Card.Body className='overflow-auto h-100'>
        {logMessages.map((message) => (
          <StockLogMessageComponent
            id={message.id}
            key={message.id}
            timestamp={message.timestamp}
          >
            {message.stocks && message.stocks.map((stock) => (
              <StockLogSymbolComponent
                symbol={stock.symbol}
                price={stock.price}
              />
            ))}
          </StockLogMessageComponent>
        ))}
      </Card.Body>
    </Card>
  )
}
