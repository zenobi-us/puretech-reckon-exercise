import React from 'react'
import { Card, Table } from 'react-bootstrap'

export interface IStockSummaryItem {
  symbol: string
  starting: number
  highest: number
  lowest: number
  current: number
}

export interface IStockSummaryProps {
  stocks?: IStockSummaryItem[]
}

export const StockSummaryComponent:React.FC<IStockSummaryProps> = (props) => {
  const {
    stocks = []
  } = props

  return (
    <Card>
      <Card.Header className='d-flex'>
        <h4 className='mr-auto'>Summary</h4>
      </Card.Header>
      <Card.Body>
        <Table>
          <thead>
            <tr>
              <th scope='col'>symbol</th>
              <th scope='col'>Starting</th>
              <th scope='col'>Lowest</th>
              <th scope='col'>Highest</th>
              <th scope='col'>Current</th>
            </tr>
          </thead>
          <tbody>
            {stocks.map((stock) => (
              <tr>
                <th scope='row'>{stock.symbol}</th>
                <td>{stock.starting}</td>
                <td>{stock.lowest}</td>
                <td>{stock.highest}</td>
                <td>{stock.current}</td>
              </tr>
            ))}
          </tbody>
        </Table>
      </Card.Body>
    </Card>
  )
}
