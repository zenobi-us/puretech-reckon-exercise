import { observer } from 'mobx-react'
import React from 'react'

import { useStore } from '../store'

import { StockLogComponent } from './StockLogComponent'

export const StockLogContainer = observer(
  function StockLogContainer () {
    const store = useStore()
    const quotes = store?.log
    const isLogging = store?.isLogging

    function handlePauseToggleStockQuoteLog () {
      if (typeof store?.toggleLogs !== 'function') return
      store.toggleLogs()
    }

    return (
      <StockLogComponent
        messages={quotes}
        isLogging={isLogging}
        onPauseToggleClick={handlePauseToggleStockQuoteLog}
      />
    )
  }
)
