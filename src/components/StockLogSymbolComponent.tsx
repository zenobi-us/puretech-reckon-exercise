import React from 'react'
import { Badge, ListGroup } from 'react-bootstrap'

import { StockModelDTO } from '../store/Quote/StockModel'


export const StockLogSymbolComponent:React.FC<StockModelDTO> = ({
  symbol,
  price,
}) => {
  return (
    <ListGroup.Item
      className='d-flex align-items-center justify-content-between'
    >
      {symbol} <Badge variant='primary'>{price}</Badge>
    </ListGroup.Item>
  )
}
