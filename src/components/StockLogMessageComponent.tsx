import React from 'react'
import { ListGroup, ListGroupItem } from 'react-bootstrap'

import { LogModelDTO } from '../store/Quote/LogModel'

export interface IStockLogMessageProps extends Omit<LogModelDTO, 'relatedStocks' | 'stocks' | 'display'> {
  id: number
}

export const StockLogMessageComponent:React.FC<IStockLogMessageProps> = ({
  id,
  timestamp,
  children,
}) => {

  return (
    <ListGroup className='mb-4'>
      <ListGroupItem variant='primary'>
        <div className='d-flex w-100 justify-content-between'>
          <h5 className='mb-1'>Updates for {timestamp.toISOString()}</h5>
          <small>{id}</small>
        </div>
      </ListGroupItem>
      {children}
    </ListGroup>
  )
}

StockLogMessageComponent.displayName = 'StockLogMessageComponent'