import React from 'react'
import { Container, Col, Row } from 'react-bootstrap'

export interface IAppProps {
  logSlot: React.ReactNode,
  summarySlot: React.ReactNode,
}

export const AppComponent:React.FC<IAppProps> = ({
  logSlot,
  summarySlot
}) => {
  return (
    <Container fluid='lg' className='p-4 vh-100 justify-items-stretch d-flex vw-100'>
      <Row className='w-100'>
        <Col className='h-100'>
          {logSlot}
        </Col>
        <Col>
          {summarySlot}
        </Col>
      </Row>
    </Container>
  )
}

AppComponent.displayName = 'AppComponent'