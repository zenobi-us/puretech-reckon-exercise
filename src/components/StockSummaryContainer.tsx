import { observer } from 'mobx-react'
import React from 'react'
import { LoggerFactory } from 'services/LoggerFactory'

import { useStore } from '../store'

import { StockSummaryComponent } from './StockSummaryComponent'

const log = LoggerFactory(module.id)

export const StockSummaryContainer = observer(
  function StockSummaryContainer () {
    const store = useStore()
    log('summary', store.summary)
    return (
      <StockSummaryComponent stocks={store.summary} />
    )
  }
)
