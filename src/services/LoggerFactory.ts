import debug, { Debugger } from 'debug'

export function LoggerFactory (namespace: string) : Debugger {
  return debug(`reckon-stocklogger/${namespace}`)
}
