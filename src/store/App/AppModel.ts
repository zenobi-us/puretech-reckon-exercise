import {
  model,
  Model,
  modelAction,
  prop
} from 'mobx-keystone'
import Axios from 'axios'
import { computed } from 'mobx'
import { LoggerFactory } from 'services/LoggerFactory'

import { IStockSummaryItem } from '../../components/StockSummaryComponent'
import { StockModel } from '../Quote/StockModel'
import { LogModel } from '../Quote/LogModel'
import { stockModelFactory } from '../Quote/StockModelFactory'
import { logModelFactory } from '../Quote/LogModelFactory'

const log = LoggerFactory(module.id)

export const APP_MODEL_KEY = 'AppModel'

export type IAccumulatedStockItemSummary = Omit<IStockSummaryItem, 'symbol'>
export interface IAccumulatedStockSummary {
  [stockSymbol: string]: IAccumulatedStockItemSummary
}
export type IAccumulatedStockSummaryMap = IStockSummaryItem[]

export interface IStockApiResponseItemDTO {
  code: string,
  price: number
}

export interface IStockApiResponseDTO {
  data: IStockApiResponseItemDTO[]
}

@model(APP_MODEL_KEY)
export class AppModel extends Model({
  logs: prop<LogModel[]>(() => []),
  isLogging: prop<boolean>(() => true),
  stocks: prop<StockModel[]>(() => []),
  interval: prop<number>(() => 2000)
}) {

  calculateStockSummary (symbol: string): IAccumulatedStockItemSummary {
    log(symbol)
    const prices = this.stocks
      .filter((stock) => stock.symbol === symbol)
      .map((stock) => stock.price)

    return {
      highest: Math.max(...prices),
      lowest: Math.min(...prices),
      starting: prices[0],
      current: prices[prices.length - 1],
    }
  }

  @computed
  get summary (): IAccumulatedStockSummaryMap {
    const stockSymbols: string[] = this.stocks.map((stock) => stock.symbol)
    const result: IAccumulatedStockSummary = {}
    const summary = Array.from(new Set(stockSymbols))
      .reduce((result, symbol: string) => {
        return {
          ...result,
          [symbol]: this.calculateStockSummary(symbol)
        }
      }, result)

    return Object.keys(summary)
      .map((symbol: string) => ({
        symbol,
        ...summary[symbol]
      }))
  }

  @computed
  get log (): LogModel[] {
    return this.logs
      .slice()
      .sort((prev, next) =>
        prev.timestamp > next.timestamp
          ? -1
          : 1
      )
      .filter((logMessage) => logMessage.display)
  }

  @modelAction
  startLogging (): void {
    this.isLogging = true
  }

  @modelAction
  stopLogging (): void {
    this.isLogging = false
  }

  @modelAction
  toggleLogs (): void {
    this.isLogging = !this.isLogging
  }

  @modelAction
  watch (): void {
    window.setTimeout(() => {
      this.fetch()
        .catch(() => log('error'))
        .then(() => log('success'))
        .finally(() => this.watch())
    }, this.interval)
  }

  @modelAction
  fetch (): Promise<void> {
    log('fetching')
    return Axios.get<IStockApiResponseItemDTO[]>('https://join.reckon.com/stock-pricing')
      .then((response) => {
        const { data } = response
        const id = this.logs.length + 1
        const stocks = data.map((stock) => stockModelFactory(stock))
        this.addStocks(stocks)
        const logMessage = logModelFactory(id, this.isLogging, stocks)
        this.addLog(logMessage)
      })
  }
  @modelAction
  addStocks (stocks: StockModel[]): void {
    this.stocks = this.stocks.concat(stocks)
  }

  @modelAction
  addLog (log: LogModel): void {
    this.logs.push(log)
  }

}