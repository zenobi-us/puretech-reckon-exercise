import { AppModel } from './AppModel'

export interface IAppModelFactoryProps {
  watch?: boolean
}

export function appModelFactory (props: IAppModelFactoryProps = {}): AppModel {
  const {
    watch = false
  } = props
  const store = new AppModel({})
  if (watch) {
    store?.watch()
  }
  return store
}