import { registerRootStore } from 'mobx-keystone'
import { createContext, useContext } from 'react'

import { LoggerFactory } from '../services/LoggerFactory'

import { AppModel } from './App/AppModel'
import { appModelFactory } from './App/AppModelFactory'

const log = LoggerFactory(module.id)

export interface IStoreFactoryProps {
  watch?: boolean
}

export function rootStoreFactory ({ watch }: IStoreFactoryProps): AppModel {
  const store = appModelFactory({ watch })
  registerRootStore(store)
  log('StoreFactory.created', store)
  return store
}

export const StoreContext = createContext<AppModel>(appModelFactory())

export const StoreProvider = StoreContext.Provider

export function useStore (): AppModel {
  return useContext(StoreContext)
}
