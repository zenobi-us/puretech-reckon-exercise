import {
  model,
  Model,
  ModelInstanceData,
  prop,
  Ref,
  detach,
  rootRef,
} from 'mobx-keystone'

export const STOCK_MODEL_KEY = 'StockModel'

export type StockModelDTO = ModelInstanceData<StockModel>
export type StockModelReference = Ref<StockModel>

@model(STOCK_MODEL_KEY)
export class StockModel extends Model({
  symbol: prop<string>(),
  price: prop<number>()
}){

  createReference (): StockModelReference {
    return stockModelReferenceFactory(this)
  }

}

export const stockModelReferenceFactory = rootRef<StockModel>(STOCK_MODEL_KEY, {
  onResolvedValueChange (ref, newStock, oldStock) {
    if (oldStock && !newStock) {
      detach(ref)
    }
  }
})
