import {
  model,
  Model,
  ModelInstanceData,
  prop,
  prop_dateTimestamp,
  Ref,
} from 'mobx-keystone'

import { StockModel } from './StockModel'

export const LOG_MODEL_KEY = 'LogModel'
export interface LogModelDTO extends ModelInstanceData<LogModel> {
  readonly stocks: StockModel[]
}

@model(LOG_MODEL_KEY)
export class LogModel extends Model({
  id: prop<number>(),
  display: prop<boolean>(() => false),
  timestamp: prop_dateTimestamp(() => new Date()),
  relatedStocks: prop<Ref<StockModel>[]>(() => [])
}){

  get stocks (): StockModel[] {
    return this.relatedStocks.map((stockReference) => stockReference.current)
  }

}