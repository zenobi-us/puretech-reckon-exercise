import { StockModel } from './StockModel'

export interface IStockDto {
  code: string,
  price: number
}

export function stockModelFactory (stock: IStockDto): StockModel {
  return new StockModel({
    symbol: stock.code,
    price: stock.price
  })
}