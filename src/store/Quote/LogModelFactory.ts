import { LoggerFactory } from '../../services/LoggerFactory'

import { LogModel } from './LogModel'
import { StockModel } from './StockModel'

const log = LoggerFactory(module.id)

export function logModelFactory (
  id: number,
  shouldDisplay: boolean,
  stocks: StockModel[]
): LogModel {
  log('create', id, stocks)
  return new LogModel({
    id,
    display: shouldDisplay,
    timestamp: new Date(),
    relatedStocks: stocks.map(
      (stock) => stock.createReference()
    )
  })
}