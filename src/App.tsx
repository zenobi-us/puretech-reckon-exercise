import * as React from 'react'

import { rootStoreFactory, StoreProvider } from './store'
import { AppContainer } from './components/AppContainer'

const store = rootStoreFactory({ watch: true })

export const App: React.FC = () => {
  return (
    <StoreProvider value={store}>
      <AppContainer />
    </StoreProvider>
  )
}